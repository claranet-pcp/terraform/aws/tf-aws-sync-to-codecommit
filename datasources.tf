data "aws_region" "current" {}

data "aws_caller_identity" "current" {}

data "aws_kms_key" "kms" {
  key_id = "${var.kms_key_id}"
}

data "aws_codecommit_repository" "cc_repo" {
  repository_name = "${var.codecommit_repository_name}"
}
