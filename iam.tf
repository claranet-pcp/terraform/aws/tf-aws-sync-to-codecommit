# Create the IAM role for CodeBuild
resource "aws_iam_role" "codebuild" {
  description        = "${var.codebuild_name}-codebuild-role"
  assume_role_policy = "${data.aws_iam_policy_document.codebuild_assume_role.json}"
}

data "aws_iam_policy_document" "codebuild_assume_role" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["codebuild.amazonaws.com"]
    }
  }
}

# Create the IAM policy for CodeBuild.
resource "aws_iam_policy" "codebuild" {
  path        = "/service-role/"
  description = "${var.codebuild_name}-codebuild-policy"
  policy      = "${data.aws_iam_policy_document.codebuild.json}"
}

data "aws_iam_policy_document" "codebuild" {
  # Allow CodeBuild to log to CloudWatch.
  statement {
    effect    = "Allow"
    resources = ["*"]

    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]
  }

  # Allow CodeBuild to access S3 bucket where buildspec is stored
  statement {
    effect = "Allow"

    actions = [
      "s3:GetObject",
      "s3:GetObjectVersion",
      "s3:GetBucketVersioning",
    ]

    resources = [
      "arn:aws:s3:::${aws_s3_bucket_object.source.bucket}",
      "arn:aws:s3:::${aws_s3_bucket_object.source.bucket}/${aws_s3_bucket_object.source.key}",
    ]
  }

  # Allow codebuild to access codecommit
  statement {
    effect = "Allow"

    actions = [
      "codecommit:GitPull",
      "codecommit:GitPush",
    ]

    resources = ["${data.aws_codecommit_repository.cc_repo.arn}"]
  }

  # Allow codebuild to read SSM parameter
  statement {
    effect = "Allow"

    actions = [
      "ssm:GetParameter",
    ]

    resources = [
      "${local.deploy_key_parameter_arn}",
      "${local.host_key_parameter_arn}",
    ]
  }

  # allow to decrypt
  statement {
    effect = "Allow"

    actions = [
      "kms:Decrypt",
    ]

    resources = ["${data.aws_kms_key.kms.arn}"]
  }
}

# Attach the policy to the role.
resource "aws_iam_policy_attachment" "codebuild" {
  name       = "${var.codebuild_name}-codebuild-policy-attachment"
  policy_arn = "${aws_iam_policy.codebuild.arn}"
  roles      = ["${aws_iam_role.codebuild.id}"]
}

### CloudWatch

# Create IAM role for CloudWatch
resource "aws_iam_role" "cloudwatch" {
  description        = "${var.codebuild_name}-codebuild-role"
  assume_role_policy = "${data.aws_iam_policy_document.cloudwatch_assume_role.json}"
}

data "aws_iam_policy_document" "cloudwatch_assume_role" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["events.amazonaws.com"]
    }
  }
}

# Create the IAM policy for CloudWatch.
resource "aws_iam_policy" "cloudwatch" {
  path        = "/service-role/"
  description = "${var.codebuild_name}-cloudwatch-policy"
  policy      = "${data.aws_iam_policy_document.cloudwatch.json}"
}

data "aws_iam_policy_document" "cloudwatch" {
  # Allow CloudWatch to log to CloudWatch.
  statement {
    effect = "Allow"

    actions = [
      "codebuild:StartBuild",
    ]

    resources = ["${aws_codebuild_project.reposync.id}"]
  }
}

# Attach the policy to the role.
resource "aws_iam_policy_attachment" "cloudwatch" {
  name       = "${var.codebuild_name}-cloudwatch-policy-attachment"
  policy_arn = "${aws_iam_policy.cloudwatch.arn}"
  roles      = ["${aws_iam_role.cloudwatch.id}"]
}
