resource "aws_cloudwatch_event_rule" "rule" {
  description         = "Trigger Repository Sync to CodeCommit repo ${var.codecommit_repository_name}"
  schedule_expression = "${var.schedule_expression}"
}

resource "aws_cloudwatch_event_target" "target" {
  rule     = "${aws_cloudwatch_event_rule.rule.name}"
  arn      = "${aws_codebuild_project.reposync.id}"
  role_arn = "${aws_iam_role.cloudwatch.arn}"
}
