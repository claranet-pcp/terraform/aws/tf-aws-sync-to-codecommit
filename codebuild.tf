resource "aws_codebuild_project" "reposync" {
  name          = "${var.codebuild_name}"
  build_timeout = "60"
  service_role  = "${aws_iam_role.codebuild.arn}"
  tags          = "${var.tags}"

  source {
    type     = "S3"
    location = "${aws_s3_bucket_object.source.bucket}/${aws_s3_bucket_object.source.key}"
  }

  artifacts {
    type = "NO_ARTIFACTS"
  }

  environment {
    compute_type = "BUILD_GENERAL1_SMALL"
    image        = "aws/codebuild/ubuntu-base:14.04"
    type         = "LINUX_CONTAINER"

    environment_variable {
      name  = "DEPLOY_KEY_PARAMETER_NAME"
      value = "${var.deploy_key_parameter_name}"
    }

    environment_variable {
      name  = "CODECOMMIT_CLONE_URL_HTTP"
      value = "${data.aws_codecommit_repository.cc_repo.clone_url_http}"
    }

    environment_variable {
      name  = "SOURCE_REPO_HOST_SSH_PUBLIC_KEY"
      value = "${var.source_host_ssh_pubkey_parameter_name}"
    }

    environment_variable {
      name  = "SOURCE_REPO_LOCATION"
      value = "${var.source_repository_location}"
    }
  }
}
