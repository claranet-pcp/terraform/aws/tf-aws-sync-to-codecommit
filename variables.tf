variable "codecommit_repository_name" {
  description = "CodeCommit repo to sync from"
  type        = "string"
}

variable "source_repository_location" {
  description = "url of the repo to sync from"
  type        = "string"
}

variable "deploy_key_parameter_name" {
  description = "The name of the parameter key where encrypted deploy key is stored"
  type        = "string"
}

variable "source_host_ssh_pubkey_parameter_name" {
  description = "The name of the parameter key where source repo's host public ssh key is stored. Can be obtained using ssh-keyscan -H <host> command."
  type        = "string"
}

variable "codebuild_name" {
  type = "string"
}

variable "kms_key_id" {
  description = "KMS key ID used for encrypting parameter"
  type        = "string"
  default     = "alias/aws/ssm"
}

variable "tags" {
  type = "map"
}

variable "schedule_expression" {
  type        = "string"
  description = "Schedule expression for sync job"
  default     = "rate(2 minutes)"
}

# if deploy_key_parameter_arn not given we can produce one
locals {
  deploy_key_parameter_arn = "${format("arn:aws:ssm:%s:%s:parameter%s", "${data.aws_region.current.name}", "${data.aws_caller_identity.current.account_id}", "${var.deploy_key_parameter_name}")}"
  host_key_parameter_arn   = "${format("arn:aws:ssm:%s:%s:parameter%s", "${data.aws_region.current.name}", "${data.aws_caller_identity.current.account_id}", "${var.source_host_ssh_pubkey_parameter_name}")}"
}
