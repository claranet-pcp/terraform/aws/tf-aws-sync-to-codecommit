# Sync your repository to CodeCommit

This module will sync your external repository to CodeCommit.

## Prerequisites  
- existing external repository
- CodeCommit repository
- two SSM "SecureString" parameters containing
    - the deploy key for external repository
    - the public SSH host keys of a source repository host

## Usage

- send deploy key to SSM:

```bash
aws ssm put-parameter --name '/codedeploy/reposync/deploykey' --type "SecureString" --value "$(cat deploy.key)"
```

- get source host's SSH host key:

```bash
ssh-keyscan -H <host> > /tmp/pubkey
aws ssm put-parameter --name '/codedeploy/reposync/hostkey' --type "SecureString" --value "$(cat /tmp/pubkey)"
```

```hcl
resource "aws_codecommit_repository" "reposync" {
  repository_name = "synctest"
  description     = "This is the Sample Repository"
}

module "reposync" {
  source                                = "./tf-aws-sync-to-codecommit"
  codebuild_name                        = "reposync-to-cc"
  codecommit_repository_name            = "synctest"
  source_repository_location            = "git@gogs.bashton.net:pawel/test-cc.git"
  deploy_key_parameter_name             = "/codedeploy/reposync/deloykey"
  source_host_ssh_pubkey_parameter_name = "/codedeploy/reposync/hostkey"
  schedule_expression                   = "rate(1 minute)"

  tags = {
    name = "reposync"
  }
}
```
