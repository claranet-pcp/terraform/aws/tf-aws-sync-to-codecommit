resource "aws_s3_bucket" "codebuild" {
  acl = "private"

  versioning {
    enabled = true
  }
}

data "archive_file" "source" {
  type        = "zip"
  source_file = "${path.module}/buildspec.yml"
  output_path = ".terraform/terraform-aws-s3-yum-repo-${var.codebuild_name}.zip"
}

resource "aws_s3_bucket_object" "source" {
  bucket = "${aws_s3_bucket.codebuild.bucket}"
  key    = "source.zip"
  source = "${data.archive_file.source.output_path}"
  etag   = "${data.archive_file.source.output_md5}"
}
